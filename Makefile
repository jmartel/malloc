NAME	= malloc

CC		= gcc

OS		= $(shell uname -s)

PWD = \"$(shell pwd)\"
ECHO = echo
MKDIR = mkdir

DEBUG ?= 0

SRCDIR   = srcs
OBJDIR   = objs/
BINDIR   = .

INCLUDESDIR = includes
LIBFTDIR = libft

VPATH		= $(INCLUDESDIR) \
		  $(SRCDIR)/

LIBFT = $(LIBFTDIR)/libft.a

CFLAGS = -Wall -Werror -Wextra -fPIC

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

################################################################
########					MALLOC 					########
################################################################
SRCS			 =	holder.c debug.c \
					tools_block.c tools_heap.c \
					malloc.c malloc_normal.c malloc_large.c \
					free.c free_normal.c free_large.c \
					realloc.c \
					calloc.c


OBJECTS			=	$(addprefix $(OBJDIR), $(SRCS:.c=.o))
INC 			=	-I $(INCLUDESDIR) -I $(LIBFTDIR)

EOC = \033[0m
OK_COLOR = \033[1;32m
FLAGS_COLOR = \033[1;34m

LFLAGS = -L $(LIBFTDIR) -lft

SPEED = -j8

all:
	@$(MAKE) -C $(LIBFTDIR) $(SPEED)
	@$(ECHO) "$(FLAGS_COLOR)Compiling with flags $(CFLAGS) $(EOC)"
	@$(MAKE) libft_malloc_$(HOSTTYPE).so $(SPEED)

(LIBFT):
	@$(MAKE) -C $(LIBFTDIR)

libft_malloc_$(HOSTTYPE).so: $(LIBFT) $(OBJDIR) $(OBJECTS)
	@$(CC) -shared -o $@ $(OBJECTS) $(CFLAGS) $(LFLAGS)
	@$(ECHO) "$(OK_COLOR)$(NAME) linked .so with success !$(EOC)"

$(BINDIR)/$(NAME): $(LIBFT) $(OBJDIR) $(OBJECTS)
	@$(CC) -o $@ $(OBJECTS) $(CFLAGS) $(LFLAGS)
	@$(ECHO) "$(OK_COLOR)$(NAME) linked with success !$(EOC)"

$(OBJDIR):
	@$(MKDIR) $@

$(OBJDIR)%.o: $(SRC_DIR)%.c $(INCLUDES)
	@$(CC) -c $< -o $@ -I $(INCLUDESDIR) -I $(LIBFTDIR) $(CFLAGS)
	@$(ECHO) "${COMP_COLOR}$< ${EOC}"

header:
	@python3 ./.cheader.py

clean:
	@$(MAKE) clean -C $(LIBFTDIR)
	@$(RM) $(OBJECTS)
	@$(RM) -r $(OBJDIR) && $(ECHO) "${OK_COLOR}Successfully cleaned $(NAME) objects files ${EOC}"

fclean: clean
	@$(MAKE) fclean -C $(LIBFTDIR)
	@$(RM) $(BINDIR)/$(NAME)  && $(ECHO) "${OK_COLOR}Successfully cleaned $(NAME) ${EOC}"

re: fclean all

