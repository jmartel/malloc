/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   holder.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 18:36:47 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/22 14:51:41 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"
#include "ft_malloc_size.h"
#include <unistd.h>

// TODO TODEL
const enum e_debug debug_level = LEVEL_TWO;

static t_bool	initialize_holder(t_holder *holder)
{
	holder->tiny_heap = create_new_heap(TINY, NULL, NULL);
	holder->small_heap = create_new_heap(SMALL, NULL, NULL);
	if (!holder->tiny_heap || !holder->small_heap)
		return FALSE;
	holder->big_heap = NULL;
	return TRUE;
}

t_holder		*get_holder()
{
	static t_holder holder = { NULL, NULL, NULL };

	if (holder.tiny_heap == NULL)
	{
		if (!initialize_holder(&holder)) {
			ft_putstrn("get_holder: Unable to initialize holder");
			return NULL;
		}
	}
	return &holder;
}
