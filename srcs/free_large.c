/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_large.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 18:07:34 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/21 18:39:12 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"

void		handle_large_free(t_heap *heap, void *ptr)
{
	size_t	size;
	t_block	*block;
	t_heap *current_heap;
	t_heap	*next_heap;
	t_heap	*prev_heap;

	block = (t_block*)(ptr - sizeof(t_block));
	current_heap = ((void*)block) - sizeof(t_heap);
	size = block->size + sizeof(t_heap) + sizeof(t_block);
	next_heap = heap->next;
	prev_heap = heap->prev;
	if (munmap(current_heap, size)) {
		ft_putstrn("munmap returned an error !!!!!!");
		return ;
	}
	if (next_heap)
		next_heap->prev = prev_heap;
	if (prev_heap)
		prev_heap->next = next_heap;
	get_holder()->big_heap = prev_heap ? prev_heap : next_heap;
	return ;
}
