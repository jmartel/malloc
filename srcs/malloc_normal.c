/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_allocation.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 17:17:08 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/20 17:35:27 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

static void	*allocate_block(t_block *block, size_t size)
{
	// if block already have good size just mark it as used
	if (block->size == size)
	{
		block->freed = FALSE;
		return block;
	}

	// split this block in two blocks, first one allocated, second one freed
	t_block		*new_block;
	t_block		*next_block;

	new_block = block;
	next_block = (t_block*)(((void*)block) + sizeof(t_block) + size);

	next_block->size = new_block->size - size - sizeof(t_block);
	next_block->prev = new_block;
	next_block->freed = TRUE;

	new_block->size = size;
	new_block->freed = FALSE;
	return (((void*)new_block) + sizeof(t_block));
}

void	*handle_small_allocations(t_heap *heap, t_heap_type heap_type, size_t size)
{
	t_block	*block;

	do {
		block = get_first_heap_block(heap);
		while (block != NULL)
		{
			if (block->freed && block->size >= size)
				return allocate_block(block, size);
			block = get_next_heap_block(heap, heap_type, block);
		}
		if (heap->next)
			heap = heap->next;
	} while (heap->next);

	if (!(heap = extend_heap(heap_type, heap)))
		return (NULL);
	return allocate_block(get_first_heap_block(heap), size);
}
