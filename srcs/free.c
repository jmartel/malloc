/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 18:22:57 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/21 21:36:17 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"

static t_heap	*find_heap_for_ptr(t_heap *heap, t_heap_type heap_type, t_block *block)
{
	void		*first_heap_block;
	void		*heap_end;
	
	while (heap)
	{
		// start of heap + size of heap header
		first_heap_block = ((void*)heap) + sizeof(t_heap);
		// start of heap (first_block - sizeof block) + total_heap_size - last_block header
		heap_end = first_heap_block + (get_heap_size(heap_type) - sizeof(t_heap) - sizeof(t_block));
		if (first_heap_block <= (void*)block && (void*)block <= heap_end)
			return (heap);
		heap = heap->next;
	}
	return (NULL);
}

void	free(void *ptr)
{
	t_heap		*heap;

	if (debug_level >= LEVEL_TWO) {
		ft_putstrn(RED);
		ft_putstr("freeing: ");
		ft_putptr(ptr);
		ft_putstrn(EOC);
	}
	
	if (!ptr)
		return ;
	if ((long)ptr % 16 != 0) {
		// TODO maybe this must be shown all the time (as real free)
		if (debug_level >= LEVEL_ONE) {
			ft_putstrn(BRED);
			ft_putstr("Ptr to free is not aligned: ");
			ft_putptr(ptr);
			ft_putstr(EOC);
			ft_putstr("\n");
		}
		return ;
	}
	if (get_heap_by_type(TINY) && (heap = find_heap_for_ptr(get_heap_by_type(TINY), TINY, ptr)))
		handle_normal_free(heap, TINY, ptr);
	else if (get_heap_by_type(SMALL) && (heap = find_heap_for_ptr(get_heap_by_type(SMALL), SMALL, ptr)))
		handle_normal_free(heap, SMALL, ptr);
	else if (get_heap_by_type(BIG) && (heap = find_heap_for_ptr(get_heap_by_type(BIG), BIG, ptr)))
		handle_large_free(heap, ptr);
	else {
		if (debug_level >= LEVEL_ONE) {
			ft_putstrn(BRED);
			ft_putstr("Unable to find ptr to free: ");
			ft_putptr(ptr);
			ft_putstr(EOC);
			ft_putstr("\n");
		}
	}

	if (debug_level >= LEVEL_THREE) {
		show_used_space();
	}
}

void	ft_free(void *ptr)
{
	free(ptr);
}
