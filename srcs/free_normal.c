/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_normal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 18:07:38 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/22 14:55:12 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_malloc.h"

void	handle_normal_free(t_heap *heap, t_heap_type heap_type, void *ptr)
{
	t_block	*block = (t_block*)(ptr - sizeof(t_block));
	t_block	*prev_block = block->prev;
	t_block	*next_block = get_next_heap_block(heap, heap_type, block);
	t_block *next_next_block;

	// possible to merge with previous and next block
	if (prev_block && prev_block->freed && next_block && next_block->freed) {
		next_next_block = get_next_heap_block(heap, heap_type, next_block);
		prev_block->size += 2 * sizeof(t_block) + block->size + next_block->size;
		if (next_next_block)
			next_next_block->prev = prev_block;
		if (debug_level >= LEVEL_THREE)
			ft_putstrn("free_block: double merging !");
	}
	// only possible to merge with next block
	else if (next_block && next_block->freed) {
		next_next_block = get_next_heap_block(heap, heap_type, next_block);
		block->freed = TRUE;
		block->size += sizeof(t_block) + next_block->size;
		if (next_next_block)
			next_next_block->prev = prev_block;
		if (debug_level >= LEVEL_THREE)
			ft_putstrn("free_block: merging with next");
	}
	// only possible to merge with prev block
	else if (prev_block && prev_block->freed) {
		prev_block->size += sizeof(t_block) + block->size;
		next_block->prev = prev_block;
		if (debug_level >= LEVEL_THREE)
			ft_putstrn("free_block: merging with prev");
	}
	// unable to merge just mark as free
	else {
		if (debug_level >= LEVEL_THREE)
			ft_putstrn("free_block: simple free");
		block->freed = TRUE;
	}
}
