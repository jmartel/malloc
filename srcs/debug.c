/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 20:25:29 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/22 14:47:35 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"

void	show_block(t_block *block)
{
	if (!block)
	{
		ft_putstr("NULL");
		return;
	}
	ft_putptr(block->prev);
	ft_putstr(" <- (add: ");
	ft_putptr((void*)block);
	ft_putstr(" | s:");
	ft_putnbr(block->size);
	ft_putstr(" | f:");
	ft_putnbr(block->freed);
	ft_putstr(")\n");
}

void	show_heap_blocks(t_heap *heap, t_heap_type heap_type)
{
	t_block *block;
	
	while (heap)
	{
		block = get_first_heap_block(heap);
		while (block)
		{
			show_block(block);
			block = get_next_heap_block(heap, heap_type, block);
		}
		heap = heap->next;
	}
}

void	show_heap(t_heap *heap)
{
	if (!heap)
	{
		ft_putstrn("NULL");
		return;
	}
	ft_putptr(heap->prev);
	ft_putstr(" <- (");
	ft_putptr(heap);
	ft_putstr(") -> ");
	ft_putptr(heap->next);
	ft_putstr("\n");
}

void	show_full_heap(t_heap *heap, t_heap_type heap_type)
{
	if (heap == NULL)
	{
		ft_putstrn("heap is NULL");
		return;
	}
	// ft_putstrn("==================================================================");
	while (heap)
	{
		ft_putstr("HEAP:\n");
		show_heap(heap);
		ft_putstrn("BLOCK LIST: ");
		show_heap_blocks(heap, heap_type);
		heap = heap->next;
		if (heap)
			ft_putstrn("");
	}
	// ft_putstrn("==================================================================");
}

void	show_state()
{
	t_heap	*heap;

	heap = get_heap_by_type(TINY);
	ft_putstrn("---------- TINY HEAP ----------");
	if (heap) {
		show_full_heap(heap, TINY);
	}
	else {
		ft_putstrn("NULL");
	}
	ft_putstrn("------------------------------");

	heap = get_heap_by_type(SMALL);
	ft_putstrn("---------- SMALL HEAP ----------");
	if (heap) {
		show_full_heap(heap, SMALL);
	}
	else {
		ft_putstrn("NULL");
	}
	ft_putstrn("------------------------------");

	heap = get_heap_by_type(BIG);
	ft_putstrn("---------- BIG HEAP ----------");
	if (heap) {
		while (heap) {
			show_heap(heap);
			show_block(get_first_heap_block(heap));
			heap = heap->next;
		}
	}
	else {
		ft_putstrn("NULL");
	}
	ft_putstrn("------------------------------");
}

static void	show_heap_used_space(t_heap *heap, t_heap_type heap_type)
{
	t_block	*block;
	size_t	free_space = 0;
	size_t	used_space = 0;
	int		number_of_heap = 0;
	
	if (!heap) {
		ft_putstr("number of heap: 0\n");
		return ;
	}
	while (heap)
	{
		block = get_first_heap_block(heap);
		while (block)
		{
			if (block->freed)
				free_space += block->size;
			else
				used_space += block->size;
			block = get_next_heap_block(heap, heap_type, block);
		}
		number_of_heap += 1;
		heap = heap->next;
	}
	ft_putstr("number of heap: ");
	ft_putnbr(number_of_heap);
	ft_putstr(" | used: ");
	ft_putnbr(used_space);
	ft_putstr(" | available: ");
	ft_putnbr(free_space);
	if (heap_type != BIG) {
		ft_putstr(" | heap size: ");
		ft_putnbrn(get_heap_size(heap_type));
	}
	else {
		ft_putstr("\n");
	}
}

void	show_used_space()
{
	ft_putstr("TINY: ");
	show_heap_used_space(get_heap_by_type(TINY), TINY);
	ft_putstr("SMALL: ");
	show_heap_used_space(get_heap_by_type(SMALL), SMALL);
	ft_putstr("BIG: ");
	show_heap_used_space(get_heap_by_type(BIG), BIG);
}

// void	print_memory(void *start, size_t size)
// {
// 	char	*head = start;
// 	size_t	index = 0;

// 	printf("%p | ", head);
// 	while (index < size)
// 	{
// 		printf("0x%02x ", head[index]);
// 		index++;
// 	}
// 	printf("| %p\n", ((void*)head) + index);
// }

static int		ft_nbrlen(unsigned long long l, unsigned long long lbase)
{
	int					nb;

	nb = 0;
	while (l > 0)
	{
		nb++;
		l = l / lbase;
	}
	return (nb);
}

static void		ft_lltoa_rec(unsigned long long ul,
		unsigned long long lbase, char *str)
{
	if (ul < lbase)
	{
		if (ul % lbase < 10)
			str[0] = ul % lbase + '0';
		else
			str[0] = ul % lbase + 'A' - 10;
		return ;
	}
	else
	{
		ft_lltoa_rec(ul / lbase, lbase, &str[1]);
		if (ul % lbase < 10)
			str[0] = ul % lbase + '0';
		else
			str[0] = ul % lbase + 'A' - 10;
	}
}

static char		*ft_lltoa_local(long long l, int base, char *buffer)
{
	unsigned long long	ul;
	unsigned long long	lbase;
	int					neg;
	int					len;

	if (l == 0) {
		buffer[0] = '0';
		buffer[1] = 0;
		return buffer;
	}
	neg = 1;
	len = 0;
	if (l < 0 && base == 10)
	{
		neg = -1;
		len = 1;
	}
	ul = neg * (unsigned long long)l;
	lbase = (long long)base;
	len += ft_nbrlen(ul, lbase);
	ft_lltoa_rec(ul, lbase, buffer);
	if (neg == -1)
		ft_strcat(buffer, "-");
	buffer[len] = 0;
	ft_strrev(buffer);
	ft_strtolower(buffer);
	return (buffer);
}

void	ft_putptr(void *ptr)
{
	char	buffer[64 + 1];
	
	if (ptr == NULL) {
		ft_putstr("NULL");
		return ;
	}
	ft_putstr(ft_lltoa_local((long long)ptr, 16, buffer));
}

void	ft_putptrn(void *ptr)
{
	char	buffer[64 + 1];

	if (ptr == NULL) {
		ft_putstrn("NULL");
		return ;
	}
	ft_putstr("0x");
	ft_putstrn(ft_lltoa_local((long long)ptr, 16, buffer));
}
