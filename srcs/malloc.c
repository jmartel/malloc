/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 18:22:51 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/22 14:58:48 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"

#define align8(x) (((((x)-1)>>4)<<4)+8)

void	*malloc(size_t size)
{
	t_heap_type heap_type;
	t_heap	*heap;
	void	*ret;

	if (debug_level >= LEVEL_TWO) {
		ft_putstrn(GREEN);
		ft_putstr("malloc: ");
		ft_putnbr(size);
		ft_putstrn(EOC);
	}

	// align size
	// size = align8(size);
	// size = size % 16 == 0 ? size : size + (16 - size%16);
	size = (size + 15) & ~15;
	heap_type = get_heap_type_by_size(size);
	heap  = get_heap_by_type(heap_type);
	if (heap_type == BIG)
		ret = handle_large_allocation(size);
	else if (!heap) {
		ft_putstrn("ft_malloc: heap is NULL");
		ret = NULL;
	}
	else
		ret = handle_small_allocations(heap, heap_type, size);

	if (debug_level >= LEVEL_THREE) {
		show_heap_blocks(heap, heap_type);
	}

	if (debug_level >= LEVEL_TWO) {
		ft_putstr(GREEN);
		ft_putstr("malloc return: ");
		ft_putptr(ret);
		ft_putstrn(EOC);
		ft_putstr("\n");
	}

	return ret;
}

void	*ft_malloc(size_t size)
{
	return malloc(size);
}
