/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_large.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 17:17:11 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/21 18:07:15 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "libft.h"

static t_heap	*get_last_large_heap()
{
	t_heap	*heap;

	if (!(heap = get_heap_by_type(BIG)))
		return (NULL);
	while (heap->next)
		heap = heap->next;
	return heap;
}

void	*handle_large_allocation(size_t requested_size)
{
	size_t	heap_size;
	t_heap	*previous_heap;
	t_heap	*new_heap;
	t_block	*block;

	heap_size = sizeof(t_heap) + sizeof(t_block) + requested_size;
	if (!(new_heap = allocate_new_heap(heap_size)))
		return (NULL);
	
	// fill heap structure and store in heap linked list
	previous_heap = get_last_large_heap();
	new_heap->prev = previous_heap;
	new_heap->next = NULL;
	if (previous_heap)
		previous_heap->next = new_heap;
	else
		get_holder()->big_heap = new_heap;
	
	// fill block structure
	block = get_first_heap_block(new_heap);
	block->freed = FALSE;
	block->prev = NULL;
	block->size = requested_size;

	return ((void*)block) + sizeof(t_block);
}
