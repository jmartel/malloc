/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_block.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 17:19:16 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/20 17:36:38 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"

t_block		*get_first_heap_block(t_heap *heap)
{
	return (t_block*)((void*)heap + sizeof(t_heap));
}

t_block		*get_next_heap_block(t_heap *heap, t_heap_type type, t_block *block)
{
	t_block *res = (t_block*)((void*)block + sizeof(t_block) + block->size);
	if ((void*)res >= (void*)heap + get_heap_size(type))
		return (NULL);
	return res;
}
