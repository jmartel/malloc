/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_heap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/20 17:18:15 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/21 21:14:52 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_size.h"
#include <unistd.h>
// TODO TODEL
#include "libft.h"

size_t		get_heap_size(t_heap_type type)
{
	size_t		i = 1;
	size_t		page_size = getpagesize();
	size_t		block_size = 0;

	if (type == TINY)
		block_size = TINY_BLOCK_SIZE;
	else if (type == SMALL)
		block_size = SMALL_BLOCK_SIZE;
	size_t total_heap_size = (block_size + sizeof(t_block)) * 100 + sizeof(t_heap); 
	// TODO improve this omg
	while (i * page_size <= total_heap_size)
		i++;
	return i*page_size;
}

t_heap_type	get_heap_type_by_size(size_t size)
{
	if (size <= TINY_BLOCK_SIZE)
		return (TINY);
	else if (size <= SMALL_BLOCK_SIZE)
		return (SMALL);
	else
		return (BIG);
}

t_heap	*get_heap_by_type(t_heap_type type)
{
	t_holder	*holder = get_holder();

	if (holder == NULL)
		return NULL;
	if (type == TINY)
		return holder->tiny_heap;
	else if (type == SMALL)
		return holder->small_heap;
	else
		return holder->big_heap;
}

t_heap	*allocate_new_heap(size_t heap_size)
{
	void	*raw_heap;
	int 	prot;
	int 	flags;

	prot = PROT_READ | PROT_WRITE;
	flags = MAP_ANON | MAP_PRIVATE;
	if (debug_level >= LEVEL_ONE) {
		ft_putstr(CYAN);
		ft_putstrn("Calling mmap !!!!");
		ft_putstr(EOC);
	}
	raw_heap = mmap(NULL, heap_size, prot, flags, -1, 0);
	if (raw_heap == MAP_FAILED)
		return NULL;
	return raw_heap;
}

t_heap	*create_new_heap(t_heap_type type, t_heap *prev, t_heap *next)
{
	void	*raw_heap;
	t_heap	*heap;
	t_block	*block;
	size_t	heap_size = get_heap_size(type);

	if (!(raw_heap = allocate_new_heap(heap_size)))
		return NULL;
	// fill heap structure
	heap = (t_heap*)raw_heap;
	heap->prev = prev;
	heap->next = next;

	// fill first heap block
	block = get_first_heap_block(heap);
	block->prev = NULL;
	block->size = heap_size - sizeof(t_heap) - sizeof(t_block);
	block->freed = TRUE;
	return heap;
}

/*
** last_heap is optional, pass last heap element (for a type) if you have it
** else it will retrieve it using heap_type
*/
t_heap			*extend_heap(t_heap_type heap_type, t_heap *last_heap)
{
	t_heap		*new_heap;

	if (!last_heap)
		last_heap = get_heap_by_type(heap_type);
	if (last_heap)
		while (last_heap->next)
			last_heap = last_heap->next;
	new_heap = create_new_heap(heap_type, last_heap, NULL);
	last_heap->next = new_heap;
	return new_heap;
}
