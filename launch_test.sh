#!/bin/bash

set -e
cd `dirname $0`

make header all
libname=libft_malloc_$(uname -m)_$(uname -s).so

cd test
gcc *.c -g3 -I ../libft -L ../libft -lft `pwd`/../$libname -o test

cd ..

#export DYLD_LIBRARY_PATH=`pwd`
#export DYLD_INSERT_LIBRARIES=$libname
#export DYLD_FORCE_FLAT_NAMESPACE=1

./test/test
