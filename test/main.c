#include <stdio.h>
#include "../includes/ft_malloc.h"
#include <stdlib.h>
#include <string.h>
#include "libft.h"

void	libft();
void 	basic1();
void 	basic2();
void 	basic2_1();
void 	basic3();
void 	large_1();

int 	main(void)
{
	// printf("address: %lu\n", sizeof(void*));
	// printf("heap: %lu\n", sizeof(t_heap));
	// printf("block: %lu\n", sizeof(t_block));

	printf("libft: \n");
	printf("--------------------------------------------------\n");
	libft();
	printf("--------------------------------------------------\n");

	// printf("basic1: \n");
	// printf("--------------------------------------------------\n");
	// basic1();
	// printf("--------------------------------------------------\n");

	// printf("basic2: \n");
	// printf("--------------------------------------------------\n");
	// basic2();
	// printf("--------------------------------------------------\n");

	// printf("basic2.1: \n");
	// printf("--------------------------------------------------\n");
	// basic2_1();
	// printf("--------------------------------------------------\n");

	// printf("basic3: \n");
	// printf("--------------------------------------------------\n");
	// basic3();
	// printf("--------------------------------------------------\n");
	
	// printf("large_1: \n");
	// printf("--------------------------------------------------\n");
	// large_1();
	// printf("--------------------------------------------------\n");
}

void	libft()
{
	char	*str;
	t_dystr *dystr;

	if (!(dystr = ft_dystr_new("Initial string", 100, 14))) {
		printf("MALLOC IS NULL !!!!\n");
		return;
	}

	ft_putstrn("---------------- START ----------------");
	// show_heap_blocks(get_heap_by_type(TINY), TINY);
	// ft_putstrn("---------------- START ----------------");

	ft_dystr_realloc(dystr);
	// show_heap_blocks(get_heap_by_type(TINY), TINY);
	ft_dystr_realloc(dystr);
	// show_heap_blocks(get_heap_by_type(TINY), TINY);
	ft_dystr_realloc(dystr);
	// show_heap_blocks(get_heap_by_type(TINY), TINY);
	ft_dystr_realloc(dystr);
	// show_heap_blocks(get_heap_by_type(TINY), TINY);

	ft_putstrn("---------------- END ----------------");
	ft_dystr_free(dystr);
	show_used_space();
	show_state();
	ft_putstrn("---------------- END ----------------");
}

// allocate, write in and free a block
void 	basic1()
{
	int 	i;
	char 	*ptr;
	t_heap_type heap_type;

	heap_type = get_heap_type_by_size(11);
	printf("HEAP BEFORE START\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	ptr	= ft_malloc(10 * sizeof(char) + 1);
	printf("HEAP AFTER MALLOC\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	if (ptr == NULL) {
		printf("basic1: malloc returned NULL\n");
		return;
	}

	printf("heap: %p\n", get_heap_by_type(heap_type));
	printf("first block: %p\n", get_first_heap_block(get_heap_by_type(heap_type)));
	printf("ptr: %p\n", ptr);
	printf("Size of a t_heap: 0x%lx\n", sizeof(t_heap));
	printf("Size of a t_block: 0x%lx\n", sizeof(t_block));
	printf("Size of tiny heap: %zu\n", get_heap_size(heap_type));

	printf("Memory: before writting\n");
	i = 0;
	while (i < 0) {
		ptr[i] = '0' + i;
		i++;
	}
	ptr[i] = 0;
	printf("Memory: after writting\n");

	printf("model: %s\n", "0123456789");
	printf("found: %s\n", ptr);

	ft_free(ptr);
	printf("HEAP AFTER FREE\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
}

// double allocation to merge freed block with prev and next
void basic2()
{
	int 	i;
	char 	*ptr;
	char 	*ptr2;
	t_heap_type heap_type;
	size_t	allocated_size = 100;

	heap_type = get_heap_type_by_size(11);
	printf("HEAP BEFORE START\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	ptr	= ft_malloc(allocated_size * sizeof(char) + 1);
	ptr2	= ft_malloc(allocated_size * sizeof(char) + 1);
	printf("HEAP AFTER MALLOC\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	if (ptr == NULL) {
		printf("basic1: malloc returned NULL\n");
		return;
	}

	i = 0;
	while (i < allocated_size) {
		ptr[i] = '0' + i % 10;
		ptr2[i] = 'a' + i % 10;
		i++;
	}
	ptr[i] = 0;
	ptr2[i] = 0;
	printf("Memory: after writting\n");

	printf("model: %s\n", "0123456789");
	printf("found: %s\n", ptr);

	ft_free(ptr);
	printf("HEAP BETWEEN FREE\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	ft_free(ptr2);
	printf("HEAP AFTER FREE\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
}

// multiple small allocations with different size to test overflow
void basic2_1()
{
	size_t		number_of_allocations = 1000;
	size_t		max_allocated_size = 200;
	void		**allocated_elements;
	size_t		index;
	size_t		allocated_size_index;

	allocated_size_index = 1;
	allocated_elements = malloc(number_of_allocations);
	if (!allocated_elements) {
		printf("unable to allocate allocated elements");
		return ;
	}
	while (allocated_size_index <= max_allocated_size)
	{
		printf("Starting allocation: %zu\n", allocated_size_index);
		index = 0;
		while (index < number_of_allocations)
		{
			// allocate all elements and write in it
			allocated_elements[index] = ft_malloc(allocated_size_index);
			int i = 0;
			while (i < allocated_size_index) {
				((char**)allocated_elements)[index][i] = 'T';
				i++;
			}
			index++;
		}

		// free all elements (change way if odd or even)
		if (allocated_size_index % 2) {
			index = 0;
			while (index < number_of_allocations)
			{
				ft_free(allocated_elements[index]);
				index++;
			}
		}
		else {
			index = number_of_allocations - 1;
			while (index > 0)
			{
				ft_free(allocated_elements[index]);
				index--;
			}
			// index is size_t, cannot be negative, so free last manually
			ft_free(allocated_elements[index]);
		}

		// show heap state
		show_full_heap(get_heap_by_type(TINY), TINY);
		allocated_size_index++;
	}
}

// allocate blocks until a new heap is allocated
void basic3()
{
	t_heap_type heap_type = TINY;
	t_heap *heap = get_heap_by_type(heap_type);
	size_t size_to_allocate = get_heap_size(heap_type);
	size_t block_max_size = 100;
	size_t allocated_memory = 0;
	size_t number_of_allocations = 0;

	printf("block_max_size: %lu\n", block_max_size);

	// compute how many allocations we need
	while (allocated_memory < size_to_allocate) {
		size_t current_block_size = 1;
		while (allocated_memory < size_to_allocate && current_block_size < block_max_size) {
			allocated_memory += current_block_size;
			current_block_size += 1;
			number_of_allocations += 1;
		} 
	}

	printf("HEAP BEFORE START\n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	printf("number_of_allocations: %zu\n", number_of_allocations);

	// prepare memory to store all allocated addresses
	void **allocated_blocks;
	allocated_blocks = malloc(number_of_allocations * sizeof(*allocated_blocks));

	// allocate memory
	int		i = 0;
	while (i < number_of_allocations) {
		size_t current_block_size = 1;
		while (i < number_of_allocations && current_block_size < block_max_size) {
			// printf("Allocating: block number: %d | size: %zu\n\n", i, current_block_size);
			allocated_blocks[i] = ft_malloc(current_block_size * sizeof(char));
			i++;
			current_block_size++;

			// write in block
			size_t block_index = 0;
			while (block_index < current_block_size-2) {
				((char**)allocated_blocks)[i-1][block_index] = '@';
				block_index++;
			}
			((char**)allocated_blocks)[i-1][block_index] = '\0';
		}
	}
	// printf("HEAP AFTER ALLOCATIONS \n");
	// show_full_heap(get_heap_by_type(heap_type), heap_type);

	i = 0;
	while (i < number_of_allocations) {
		ft_free(allocated_blocks[i]);
		i++;
	}
	printf("HEAP AFTER FREE \n");
	show_full_heap(get_heap_by_type(heap_type), heap_type);
	free(allocated_blocks);
}

// check allocation and free for big blocks 
void large_1()
{
	char	**huge_tab;
	size_t	line = 10;
	size_t	column = get_heap_size(BIG) + 100;
	int		i;
	int		j;

	// allocate tab
	huge_tab = ft_malloc(line * sizeof(*huge_tab));
	if (!huge_tab) {
		printf("Unable to allocate tab lines");
		return;
	}

	// allocate lines
	i = 0;
	while (i < line)
	{
		if (!(huge_tab[i] = ft_malloc((column + 1) * sizeof(**huge_tab))))
			printf("Unable to allocate a line\n");
		i++;
	}

	// write in lines
	i = 0;
	j = 0;
	while (i < line)
	{
		while (j < column)
		{
			huge_tab[i][j] = 'J';
			j++;
		}
		huge_tab[i][j] = 0;
		i++;
	}

	// show state
	// show_used_space();
	// show_state();

	// free tab
	i = 0;
	while (i < line)
	{
		ft_free(huge_tab[i]);
		i++;
	}
	ft_free(huge_tab);
	show_used_space();
	show_state();

	// DO THE SAME REVERSE ORDER
	// allocate tab
	huge_tab = ft_malloc(line * sizeof(*huge_tab));
	if (!huge_tab) {
		printf("Unable to allocate tab lines");
		return;
	}

	// allocate lines
	i = line - 1;
	while (i >= 0)
	{
		if (!(huge_tab[i] = ft_malloc((column + 1) * sizeof(**huge_tab))))
			printf("Unable to allocate a line\n");
		i--;
	}

	// write in lines
	i = 0;
	j = 0;
	while (i < line)
	{
		while (j < column)
		{
			huge_tab[i][j] = 'J';
			j++;
		}
		huge_tab[i][j] = 0;
		i++;
	}

	// show state
	// show_used_space();
	// show_state();

	// free tab
	i = line - 1;
	while (i >= 0)
	{
		ft_free(huge_tab[i]);
		i--;
	}
	ft_free(huge_tab);
	show_used_space();
	show_state();
}