/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/01 16:13:24 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/21 21:19:27 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# include <sys/mman.h>

# ifndef NULL
#  define NULL (void*)0
# endif

// TODO TODEL
enum e_debug {
	NONE = 0,
	LEVEL_ONE = 1,
	LEVEL_TWO = 2,
	LEVEL_THREE = 3
};
extern const enum e_debug debug_level;

typedef struct s_holder		t_holder;
typedef enum e_heap_type	t_heap_type;
typedef struct s_heap		t_heap;
typedef struct s_block		t_block;

typedef enum 		s_bool
{
	FALSE = 0x00,
	TRUE = 0x01
}					t_bool;

typedef enum e_heap_type
{
	TINY = 0,
	SMALL = 1,
	BIG = 2
} t_heap_type;

/*
** size do not include header size (total size is heap.size + sizeof(t_heap)
*/
struct s_heap
{
	t_heap		*prev;
	t_heap		*next;
};

/*
** size do not include header size (total size is block.size + sizeof(t_block)
** used size is storing size asked by user (used when freeing block after this one)
*/
struct s_block
{
	t_block		*prev;
	size_t		freed: 1;
	size_t		size: 31;
};

struct s_holder
{
	t_heap	*tiny_heap;
	t_heap	*small_heap;
	t_heap	*big_heap;
};

/*
********************************************************************************
*/

/*
** calloc.c
*/
void				*calloc(size_t count, size_t size);

/*
** debug.c
*/
void				show_block(t_block *block);
void				show_heap_blocks(t_heap *heap, t_heap_type heap_type);
void				show_heap(t_heap *heap);
void				show_full_heap(t_heap *heap, t_heap_type heap_type);
void				show_state();
void				show_used_space();
void				ft_putptr(void *ptr);
void				ft_putptrn(void *ptr);

/*
** free.c
*/
void				free(void *ptr);
void				ft_free(void *ptr);

/*
** free_large.c
*/
void				handle_large_free(t_heap *heap, void *ptr);

/*
** free_normal.c
*/
void				handle_normal_free(
	t_heap *heap, t_heap_type heap_type, void *ptr);

/*
** holder.c
*/
t_holder			*get_holder();

/*
** malloc.c
*/
void				*malloc(size_t size);
void				*ft_malloc(size_t size);

/*
** malloc_large.c
*/
void				*handle_large_allocation(size_t requested_size);

/*
** malloc_normal.c
*/
void				*handle_small_allocations(
	t_heap *heap, t_heap_type heap_type, size_t size);

/*
** realloc.c
*/
void				*realloc(void *ptr, size_t size);

/*
** tools_block.c
*/
t_block				*get_first_heap_block(t_heap *heap);
t_block				*get_next_heap_block(
	t_heap *heap, t_heap_type type, t_block *block);

/*
** tools_heap.c
*/
size_t				get_heap_size(t_heap_type type);
t_heap_type			get_heap_type_by_size(size_t size);
t_heap				*get_heap_by_type(t_heap_type type);
t_heap				*allocate_new_heap(size_t heap_size);
t_heap				*create_new_heap(
	t_heap_type type, t_heap *prev, t_heap *next);
t_heap				*extend_heap(
	t_heap_type heap_type, t_heap *last_heap);

#endif
