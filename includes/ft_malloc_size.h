/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_size.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/02 15:24:53 by jmartel           #+#    #+#             */
/*   Updated: 2021/10/02 15:29:12 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_SIZE_H
# define FT_MALLOC_SIZE_H

# define LARGE_BLOCK_SIZE 4096
# define SMALL_BLOCK_SIZE LARGE_BLOCK_SIZE / 2
# define TINY_BLOCK_SIZE SMALL_BLOCK_SIZE / 4

#endif
