#!/bin/bash

make

libname=libft_malloc_$(uname -m)_$(uname -s).so

export DYLD_LIBRARY_PATH=`pwd`
export DYLD_INSERT_LIBRARIES=./$libname
export DYLD_FORCE_FLAT_NAMESPACE=1

$@
